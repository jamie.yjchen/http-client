//
//  HttpResponse.swift
//  TestClient
//
//  Created by Jamie Chen on 2018/5/1.
//  Copyright © 2018 Jamie Chen. All rights reserved.
//

import Foundation

struct TestingObject: Decodable {
    var info: Info
    var item: [String]
    
    struct Info: Decodable {
        var schema: String
    }
}


