//
//  TestDelegate.swift
//  TestDelegate
//
//  Created by Jamie Chen on 2018/4/26.
//  Copyright © 2018 Jamie Chen. All rights reserved.
//

import UIKit

protocol TestDelegate: class {
    func finishAction(with view: UILabel?, haveIndicator: Bool)
}

extension TestDelegate {
    func finishAction(with view: UILabel?, haveIndicator: Bool) {
        if haveIndicator {
            view?.removeFromSuperview()
        }
        
        print("finish download")
    }
}

class Downloader {
    var activeLoader: Bool = false
    weak var delegate: TestDelegate?
    private var msgLabel: UILabel?
    
    func startDownload() {
        if activeLoader {
            print("activate activity indicator")
            if let delegate = delegate, let vc = delegate as? UIViewController {
                msgLabel = UILabel()
                msgLabel!.backgroundColor = .darkGray
                msgLabel!.backgroundColor?.withAlphaComponent(0.5)
                msgLabel!.textColor = .white
                msgLabel!.text = "Running....... activate activity indicator"
                msgLabel?.textAlignment = .center
                vc.view.addSubview(msgLabel!)
                msgLabel!.frame = vc.view.frame
            }
            
        }
        
        DispatchQueue.global(qos: .default).async {
            for i in 1...100000 {
                print("downloading. \(i)")
            }
            
            DispatchQueue.main.async {
                self.didFinishDownload()
            }
        }
    }
    
    func didFinishDownload() {
        delegate?.finishAction(with: msgLabel, haveIndicator: activeLoader)
    }
}
