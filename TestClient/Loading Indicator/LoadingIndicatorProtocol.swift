//
//  LoadingIndicatorProtocol.swift
//  TestClient
//
//  Created by Jamie Chen on 2018/5/3.
//  Copyright © 2018 Jamie Chen. All rights reserved.
//

import UIKit

struct IndicatorBaseProperties {
    var activateIndicator: Bool = false
    var loadingIndicator: LoadingIndicator = LoadingIndicator()
    var message: String? = nil
    var loadingImage: UIImage? = nil
    var withBase: Bool = true
}

protocol SetIndicatorProperties {
    var setIndicatorProperties: IndicatorBaseProperties { get set }
}

protocol LoadingIndicatorProtocol: SetIndicatorProperties { }

extension LoadingIndicatorProtocol {
    var loadingImage: UIImage? {
        get { return setIndicatorProperties.loadingImage }
        set { setIndicatorProperties.loadingImage = newValue }
    }
    
    var activateIndicator: Bool {
        get { return setIndicatorProperties.activateIndicator }
        set { setIndicatorProperties.activateIndicator = newValue }
    }
    
    var loadingIndicator: LoadingIndicator {
        get { return setIndicatorProperties.loadingIndicator }
        set { setIndicatorProperties.loadingIndicator = newValue }
    }
    
    var message: String? {
        get { return setIndicatorProperties.message }
        set { setIndicatorProperties.message = newValue }
    }
    
    var withBase: Bool {
        get { return setIndicatorProperties.withBase }
        set { setIndicatorProperties.withBase = newValue}
    }
}

// 開啟/關閉 activity indicator
extension LoadingIndicatorProtocol {
    func addActivityIndicator(target vc: UIViewController) {
        
        loadingIndicator.message = message
        loadingIndicator.withBaseView = withBase
        vc.view.addSubview(loadingIndicator)
        
        loadingIndicator.translatesAutoresizingMaskIntoConstraints = false
        loadingIndicator.topAnchor.constraint(equalTo: vc.view.topAnchor).isActive = true
        loadingIndicator.bottomAnchor.constraint(equalTo: vc.view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        loadingIndicator.trailingAnchor.constraint(equalTo: vc.view.safeAreaLayoutGuide.trailingAnchor).isActive = true
        loadingIndicator.leadingAnchor.constraint(equalTo: vc.view.safeAreaLayoutGuide.leadingAnchor).isActive = true
        
        loadingIndicator.start()
    }
    
    func removeActivityIndicator() {
        loadingIndicator.stop()
        loadingIndicator.removeFromSuperview()
    }
}
