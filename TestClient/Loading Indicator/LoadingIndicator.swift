//
//  LoadingIndicator.swift
//  TestClient
//
//  Created by Jamie Chen on 2018/5/2.
//  Copyright © 2018 Jamie Chen. All rights reserved.
//

import UIKit

class LoadingIndicator: UIView {
    enum LoadingAnimatingViewStyle {
        case white
        case gray
    }
    
    var style: LoadingAnimatingViewStyle = .gray
    private var blurEffect = UIBlurEffect(style: .dark)
    private var blurView = UIVisualEffectView(effect: nil)
    private var activityIndicatorView: UIActivityIndicatorView!
    private var baseView: UIView!
    private var stackView: UIStackView!
    private var textLabel: UILabel!
    var message: String?
    var withBaseView: Bool = true
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initUI()
    }
    
    override func layoutSubviews() {
        self.setUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initUI() {
        blurView.effect = blurEffect
        blurView.frame = self.bounds
        blurView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        baseView = UIView()
        baseView.alpha = 0.75
        baseView.layer.cornerRadius = 10
        
        activityIndicatorView = UIActivityIndicatorView()
        activityIndicatorView.transform = CGAffineTransform(scaleX: 2.5, y: 2.5)
        activityIndicatorView.hidesWhenStopped = true
        activityIndicatorView.startAnimating()
        
        textLabel = UILabel()
        textLabel.textAlignment = .center
        setStyle()
        
        stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.spacing = 10
        stackView.distribution = .fillProportionally
        stackView.addArrangedSubview(activityIndicatorView)
        stackView.addArrangedSubview(textLabel)
        
        self.addSubview(blurView)
        self.addSubview(baseView)
        baseView.addSubview(stackView)
    }
    
    private func setUI() {
        let viewSize: CGFloat = 120
        blurView.frame = self.frame
        
        stackView.translatesAutoresizingMaskIntoConstraints = false
        textLabel.translatesAutoresizingMaskIntoConstraints = false
        baseView.translatesAutoresizingMaskIntoConstraints = false
        
        baseView.heightAnchor.constraint(equalToConstant: viewSize*4/3).isActive = true
        baseView.widthAnchor.constraint(equalToConstant: viewSize).isActive = true
        baseView.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        baseView.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        
        stackView.topAnchor.constraint(equalTo: baseView.topAnchor, constant: 10).isActive = true
        stackView.bottomAnchor.constraint(equalTo: baseView.bottomAnchor, constant: -10).isActive = true
        stackView.leadingAnchor.constraint(equalTo: baseView.leadingAnchor, constant: 10).isActive = true
        stackView.trailingAnchor.constraint(equalTo: baseView.trailingAnchor, constant: -10).isActive = true
        
        textLabel.heightAnchor.constraint(lessThanOrEqualToConstant: 40).isActive = true
    }
    
    func start() {
        self.isHidden = false
        self.activityIndicatorView.startAnimating()
        self.textLabel.text = message
        self.blurView.effect = self.blurEffect
        setStyle()
    }
    
    func stop() {
        self.isHidden = true
        self.activityIndicatorView.stopAnimating()
    }
    
    private func setStyle() {
        switch self.style {
        case .white:
            self.blurEffect = UIBlurEffect(style: .light)
            self.baseView.backgroundColor = .white
            self.activityIndicatorView.activityIndicatorViewStyle = .gray
            self.textLabel.textColor = .black
        case .gray:
            self.blurEffect = UIBlurEffect(style: .dark)
            self.baseView.backgroundColor = .black
            self.activityIndicatorView.activityIndicatorViewStyle = .white
            self.textLabel.textColor = .white
        }
        if !withBaseView {
            self.baseView.backgroundColor = .clear
        }
    }
}
