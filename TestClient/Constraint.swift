//
//  Constraint.swift
//  TestClient
//
//  Created by Jamie Chen on 2018/4/30.
//  Copyright © 2018 Jamie Chen. All rights reserved.
//

import Foundation

struct HTTPConstants {
    static let post = "POST"
    static let get = "GET"
    static let accept = "Accept"
    static let contentType = "Content-Type"
    static let auth = "Authorization"
    static let cookie = "cookie"
    static let jsonType = "application/json; charset=utf-8"
    static let xmlType = "application/xml"
    static let formDataType = "multipart/form-data"
}
