//
//  Encode.swift
//  TestClient
//
//  Created by Jamie Chen on 2018/4/30.
//  Copyright © 2018 Jamie Chen. All rights reserved.
//

import Foundation

func queryPair(key: String, value: Any) -> String {
    return uniEncode(str: key) + "=" + uniEncode(str: value)
}

func queryString(query: Any) -> String? {
    
    var pairs: [String]?
    if let dict = query as? Dictionary<String, Any> {
        pairs = Array()
        for (key, value) in dict {
            pairs!.append(queryPair(key: key, value: value))
        }
    } else if let array = query as? [String] {
        pairs = array
    }
    
    if let pairs = pairs {
        return pairs.joined(separator: "&")
    }
    
    return nil
}
