//
//  Decode.swift
//  TestClient
//
//  Created by Jamie Chen on 2018/4/30.
//  Copyright © 2018 Jamie Chen. All rights reserved.
//

import Foundation

func uniEncode(str: Any) -> String {
    let allowCharacters = CharacterSet(charactersIn: " \"#%/<>?@\\^`{}[]|&+").inverted
    return (str as AnyObject).addingPercentEncoding(withAllowedCharacters: allowCharacters)!
}

func dataToString(data: Data) -> String {
    return NSString(data: data, encoding: String.Encoding.utf8.rawValue)! as String
}
