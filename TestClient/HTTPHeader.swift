//
//  HttpRequest.swift
//  TestClient
//
//  Created by Jamie Chen on 2018/4/30.
//  Copyright © 2018 Jamie Chen. All rights reserved.
//

import Foundation

struct HTTPHeader {
    public enum HeaderActions: String {
        case json = "application/json; charset=utf-8"
        case xml = "application/xml"
        case formData = "multipart/form-data"
    }
    var withCookie: String?
    var accept: HeaderActions?
    var authorization: HeaderActions?
    var contentType: HeaderActions?
    
    
    init(withCookie: String?, accept: HeaderActions?, withAuthorization: HeaderActions? = nil, contentType: HeaderActions?) {
        self.withCookie = withCookie
        self.accept = accept
        self.authorization = withAuthorization
        self.contentType = contentType
    }
}
