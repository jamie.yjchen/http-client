//
//  HTTPResponseCode.swift
//  TestClient
//
//  Created by Jamie Chen on 2018/5/3.
//  Copyright © 2018 Jamie Chen. All rights reserved.
//

import UIKit

// 判別status code
extension HTTPClient {
    public enum ResponseType: Int {
        case continuing = 100                    // 100
        case switchingProtocol = 101             // 101
        case processing = 102                    // 102
        case ok = 200                            // 200
        case created = 201                       // 201
        case accepted = 202                      // 202
        case nonAuthoritativeInfo = 203          // 203
        case noContent = 204                     // 204
        case resetContent = 205                  // 205
        case partialContent = 206                // 206
        case multiStatus = 207                   // 207
        case multipleChoices = 300               // 300
        case movedPermanently = 301              // 301
        case found = 302                         // 302
        case seeOther = 303                      // 303
        case notModified = 304                   // 304
        case useProxy = 305                      // 305
        case temporaryRedirect = 307             // 307
        case badRequest = 400                    // 400
        case unauthorized = 401                  // 401
        case paymentRequired = 402               // 402
        case forbidden = 403                     // 403
        case notFound = 404                      // 404
        case methodNotAllowed = 405              // 405
        case notAcceptable = 406                 // 406
        case proxyAuthentication = 407           // 407
        case requestTimeout = 408                // 408
        case conflict = 409                      // 409
        case gone = 410                          // 410
        case lengthRequired = 411                // 411
        case preConditionFail = 412              // 412
        case requestEntityTooLarge = 413         // 413
        case requestURITooLong = 414             // 414
        case unsupportedMediaType = 415          // 415
        case requestedRangeNotSatisfiable = 416  // 416
        case expectationFailed = 417             // 417
        case authenticationTimeout = 419         // 419
        case tooManyRequests = 429               // 429
        case internalServerError = 500           // 500
        case notImplemented = 501                // 501
        case badGateway = 502                    // 502
        case serviceUnavailable = 503            // 503
        case gatewayTimeout = 504                // 504
        case httpVersionNotSupported = 505       // 505
        case unknown = 999                       // ???
    }
    
    func checkResponse(status code: HTTPURLResponse) -> ResponseType {
        switch code.statusCode {
        case 100:
            return ResponseType.continuing
        case 101:
            return ResponseType.switchingProtocol
        case 102:
            return ResponseType.processing
        case 200:
            return ResponseType.ok
        case 201:
            return ResponseType.created
        case 202:
            return ResponseType.accepted
        case 203:
            return ResponseType.nonAuthoritativeInfo
        case 204:
            return ResponseType.noContent
        case 205:
            return ResponseType.resetContent
        case 206:
            return ResponseType.partialContent
        case 207:
            return ResponseType.multiStatus
        case 300:
            return ResponseType.multipleChoices
        case 301:
            return ResponseType.movedPermanently
        case 302:
            return ResponseType.found
        case 303:
            return ResponseType.seeOther
        case 304:
            return ResponseType.notModified
        case 305:
            return ResponseType.useProxy
        case 307:
            return ResponseType.temporaryRedirect
        case 400:
            return ResponseType.badRequest
        case 401:
            return ResponseType.unauthorized
        case 402:
            return ResponseType.paymentRequired
        case 403:
            return ResponseType.forbidden
        case 404:
            return ResponseType.notFound
        case 405:
            return ResponseType.methodNotAllowed
        case 406:
            return ResponseType.notAcceptable
        case 407:
            return ResponseType.proxyAuthentication
        case 408:
            return ResponseType.requestTimeout
        case 409:
            return ResponseType.conflict
        case 410:
            return ResponseType.gone
        case 411:
            return ResponseType.lengthRequired
        case 412:
            return ResponseType.preConditionFail
        case 413:
            return ResponseType.requestEntityTooLarge
        case 414:
            return ResponseType.requestURITooLong
        case 415:
            return ResponseType.unsupportedMediaType
        case 416:
            return ResponseType.requestedRangeNotSatisfiable
        case 417:
            return ResponseType.expectationFailed
        case 419:
            return ResponseType.authenticationTimeout
        case 429:
            return ResponseType.tooManyRequests
        case 500:
            return ResponseType.internalServerError
        case 501:
            return ResponseType.notImplemented
        case 502:
            return ResponseType.badGateway
        case 503:
            return ResponseType.serviceUnavailable
        case 504:
            return ResponseType.gatewayTimeout
        case 505:
            return ResponseType.httpVersionNotSupported
        default:
            return ResponseType.unknown
        }
    }
}
