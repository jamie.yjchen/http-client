//
//  ViewController.swift
//  TestClient
//
//  Created by Jamie Chen on 2018/4/30.
//  Copyright © 2018 Jamie Chen. All rights reserved.
//

import UIKit

class ViewController: UIViewController, HTTPGetProtocol {

    override func viewDidLoad() {
        super.viewDidLoad()
//        // post test api
//        let url: String = "https://postman-echo.com/post"
//        let data: Data? = queryString(query: ["foo1":"bar", "foo2":"bar2"])?.data(using: .utf8)
        
//        // get test api
//        let url: String = "https://swapi.co/api/people/1/"
        
//        // post test api -> form data
//        let url: String = "https://postman-echo.com/transform/collection"
//        let data: [String:String] = ["from":"1", "to":"2"]
//        let objType = TestingObject.self
        
        // get request api -> array Data
        let url: String = "https://jsonplaceholder.typicode.com/users"
        let objType = [TestingObject2].self
        
        var client = HTTPClient.shared
        client.delegate = self
        client.activateIndicator = true
        client.fetch(objectModel: objType, fetch: .json, url: url, inputData: nil) {
            if let aa = $0, let bb = aa as? [TestingObject2] {
                for b in bb {
                    print(b.username+":"+b.email)
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

