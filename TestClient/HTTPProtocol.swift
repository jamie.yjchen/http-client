//
//  MethodProtocol.swift
//  TestClient
//
//  Created by Jamie Chen on 2018/4/30.
//  Copyright © 2018 Jamie Chen. All rights reserved.
//

import UIKit

protocol HTTPClientProtocol: class {
    var duration: Double { get }
}

extension HTTPClientProtocol {
    var duration: Double { return 30 }
}

protocol HTTPGetProtocol:HTTPClientProtocol { }

protocol HTTPPostProtocol:HTTPClientProtocol { }
