//
//  HttpClient.swift
//  TestClient
//
//  Created by Jamie Chen on 2018/4/30.
//  Copyright © 2018 Jamie Chen. All rights reserved.
//

import UIKit
open class HTTPClient: LoadingIndicatorProtocol {
    var setIndicatorProperties: IndicatorBaseProperties = IndicatorBaseProperties()
    
    private init() { }
    weak var delegate: HTTPClientProtocol?
    private var request: URLRequest!
    enum FetchType {
        case image
        case json
        case data
        case string
    }
    
    private static var _shared: HTTPClient?
    public static var shared: HTTPClient {
        get {
            if _shared == nil {
                DispatchQueue.global().sync(flags: .barrier) {
                    if _shared == nil {
                        _shared = HTTPClient()
                    }
                }
            }
            return _shared!
        }
    }
}

extension HTTPClient {
    private func process(header: HTTPHeader?, param: [String: [String:String]?], complete: @escaping (_ object: Data?) -> Void) {
        guard let delegate = delegate else { return }
        var urlStr = param.keys.first!
        let duration = delegate.duration
        let queryStr = queryString(query: param.values.first! as Any)
        var method = ""
        var postData: Data?
        
        
        if ((delegate as? HTTPGetProtocol) != nil) {
            method = HTTPConstants.get
            if queryStr != nil {
                urlStr += "?\(queryStr!)"
            }
        } else if ((delegate as? HTTPPostProtocol) != nil) {
            method = HTTPConstants.post
            postData = (queryStr?.data(using: .utf8))!
        } else { return }
        
        if activateIndicator, let vc = delegate as? UIViewController {
            DispatchQueue.main.async {
                self.addActivityIndicator(target: vc)
            }
        }
        request = URLRequest(url: URL(string: urlStr)!, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: duration)
        
        if let header = header {
            if let accept = header.accept?.rawValue {
                request.addValue(accept, forHTTPHeaderField: HTTPConstants.accept)
            }
            
            if let contentType = header.contentType?.rawValue {
                request.addValue(contentType, forHTTPHeaderField: HTTPConstants.contentType)
            }
            
            if let authStr = header.authorization?.rawValue {
                request.addValue(authStr, forHTTPHeaderField: HTTPConstants.auth)
            }
            
            if let cookieKey = header.withCookie {
                guard let cookies = HTTPCookieStorage.shared.cookies else { return }
                for cookie in cookies {
                    if cookieKey == cookie.name {
                        request.addValue(cookie.value, forHTTPHeaderField: HTTPConstants.cookie)
                    }
                }
            }
        }
        
        request.httpMethod = method
        if let inputData = postData {
            request.httpBody = inputData
        }
        
        let task = URLSession.shared.dataTask(with: request!) {
            if self.activateIndicator {
                DispatchQueue.main.async {
                    self.removeActivityIndicator()
                }
            }
            
            if $2 != nil {
                complete(nil)
                return
            }
            
            if let response = $1 as? HTTPURLResponse {
                if response.statusCode == ResponseType.ok.rawValue {
                    complete($0)
                } else {
                    print("code error = \(response.statusCode)")
                }
            }
        }
        task.resume()
    }
    
    func fetch<T: Decodable>(header: HTTPHeader? = nil, objectModel: T.Type? = nil, fetch: FetchType, url: String, inputData: [String:String]?, complete: @escaping (_ object: Any?) -> Void) {
        self.process(header: header, param: [url: inputData]) {
            if let receivedData = $0 {
                switch fetch {
                case .image:
                    complete(UIImage(data: receivedData))
                case .json:
                    if let objModel = objectModel {
                        do {
                            let model = try JSONDecoder().decode(objModel, from: receivedData)
                            complete(model)
                        } catch {
                            print(error.localizedDescription)
                            complete(nil)
                        }
                    }
                case .data:
                    complete(receivedData)
                case .string:
                    complete( String(data: receivedData, encoding: .utf8))
                }
            }
        }
    }
}
